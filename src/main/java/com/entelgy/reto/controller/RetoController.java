package com.entelgy.reto.controller;

import com.entelgy.reto.model.CommentResponse;
import com.entelgy.reto.service.CommentServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RetoController {

    private final CommentServiceImpl commentService;

    /**
     * Post Mapping para obtener los Comments
     * @return CommentResponse
     */
    @PostMapping(value = "/comments", produces = MediaType.APPLICATION_JSON_VALUE)
    public CommentResponse comments() {
        return commentService.getCommentResponse();
    }


}
