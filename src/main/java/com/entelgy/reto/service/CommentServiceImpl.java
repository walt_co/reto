package com.entelgy.reto.service;

import com.entelgy.reto.model.CommentResponse;
import com.entelgy.reto.model.api.ApiCommentResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final ApiConsumerServiceImpl apiConsumer;

    /**
     * Método que obtiene el valor del Api y lo transforma para devolver el valor en el RestController
     * @return CommentResponse
     */
    public CommentResponse getCommentResponse() {
        List<ApiCommentResponse> apiCommentResponseList = getApiResponse();
        List<String> listOfComments = getListOfStringOfApiCommentResponse(apiCommentResponseList);
        return new CommentResponse(listOfComments);
    }

    /**
     * Método que transforma una lista de ApiCommentResponse a un Listado de String
     * @param apiCommentResponseList
     * @return List<String>
     */
    public List<String> getListOfStringOfApiCommentResponse(List<ApiCommentResponse> apiCommentResponseList) {
        return apiCommentResponseList.stream()
                .map(ApiCommentResponse::getComment)
                .collect(Collectors.toList());
    }

    /**
     * Método que consume el Api
     * @return
     */
    public List<ApiCommentResponse> getApiResponse() {
        return apiConsumer.consumirCommentsApi();
    }
}
