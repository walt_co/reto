package com.entelgy.reto.service;

import com.entelgy.reto.model.CommentResponse;
import com.entelgy.reto.model.api.ApiCommentResponse;

import java.util.List;
import java.util.stream.Collectors;

public interface CommentService {

    CommentResponse getCommentResponse();

    List<String> getListOfStringOfApiCommentResponse(List<ApiCommentResponse> apiCommentResponseList);

    List<ApiCommentResponse> getApiResponse();
}
