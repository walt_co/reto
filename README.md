# Reto Entelgy

Servicio rest tipo POST con Spring Boot 2 que reestructura la respuesta de este servicio:

URL: https://jsonplaceholder.typicode.com/comments
Method: GET

a:
```bash
{
   data: [ "postId|id|email", "postId|id|email"  , "postId|id|email", "postId|id|email", "postId|id|email", .....]
}
```

## Installation

From the root of the project:
```bash
mvn clean install
```

##Use of this project
From the root of the project:
```bash
java -jar /target/reto-entelgy.jar
```