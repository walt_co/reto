package com.entelgy.reto.service;

import com.entelgy.reto.config.AppConfig;
import com.entelgy.reto.model.api.ApiCommentResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.client.MockRestServiceServer;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@RestClientTest(CommentServiceImpl.class)
@ContextConfiguration(classes = {AppConfig.class, ApiConsumerServiceImpl.class})
class ApiConsumerTest {

    @Autowired
    private ApiConsumerServiceImpl apiConsumer;

    @Autowired
    private MockRestServiceServer mockRestServiceServer;

    List<ApiCommentResponse> apiCommentResponseList;

    @BeforeEach
    void setUp() {
        apiCommentResponseList = Collections.singletonList(
                new ApiCommentResponse(1, 1, "Charly", "charly@gmail.com", "body"));
    }

    @Test
    void when_ApiConsumerReturnOneValue_ServiceShouldReturnOneValue() throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        mockRestServiceServer.expect(requestTo("https://jsonplaceholder.typicode.com/comments"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(apiCommentResponseList)));

        List<ApiCommentResponse> apiCommentResponses = apiConsumer.consumirCommentsApi();

        assertEquals(1, apiCommentResponses.get(0).getPostId());
        assertEquals(1, apiCommentResponses.get(0).getId());
        assertEquals("Charly", apiCommentResponses.get(0).getName());
        assertEquals("charly@gmail.com", apiCommentResponses.get(0).getEmail());
        assertEquals("body", apiCommentResponses.get(0).getBody());

    }
}