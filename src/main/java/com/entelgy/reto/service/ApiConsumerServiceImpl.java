package com.entelgy.reto.service;

import com.entelgy.reto.model.api.ApiCommentResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ApiConsumerServiceImpl implements ApiConsumerService{

    public static final String URL = "https://jsonplaceholder.typicode.com/comments";

    private final RestTemplate restTemplate;

    /**
     * Método que consume el Api de Comments
     * @return List<ApiCommentResponse>
     */
    public List<ApiCommentResponse> consumirCommentsApi() {
        ResponseEntity<List<ApiCommentResponse>> exchange = restTemplate.exchange(URL,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ApiCommentResponse>>() {
                });

        return exchange.getBody();
    }
}
