package com.entelgy.reto.service;


import com.entelgy.reto.model.api.ApiCommentResponse;

import java.util.List;

public interface ApiConsumerService {
    List<ApiCommentResponse> consumirCommentsApi();
}
